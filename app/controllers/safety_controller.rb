class SafetyController < ApplicationController

  skip_before_action :verify_authenticity_token

  # match "*path" => "safety#page_no_found", via: :all
  # safety/page_no_found
  # 处理不存在的页面请求
  def page_no_found
    #render :text => "#{ params[:path] } no found"
    # 若未指定404， 则默认 200
    render :plain => "#{ params[:path] } no found" ,  :status => 404
  end
  
  
  def page_all
    #render :text => "#{ params[:path] } no found"
    # 若未指定404， 则默认 200
    puts "记录的session为 #{session }"
    puts "收到params =#{params.to_json}"
    
    render :plain => "#{ params[:path] } thanks" ,  :status => 200
  end

  
end
